package services;

import java.util.List;
import models.Chocolate;
import dao.ChocolateDAO;

public class ChocolateService {
    private ChocolateDAO chocolateDAO;

    public ChocolateService() {
        chocolateDAO = new ChocolateDAO();
    }

    public List<Chocolate> getAllChocolates() {
        return chocolateDAO.getChocolates();
    }

    public Chocolate getChocolateById(int id) {
        Chocolate chocolate = chocolateDAO.findChocolate(id);
        if (chocolate == null) {
            // Handle the case when chocolate is not found
            throw new IllegalArgumentException("Chocolate with id " + id + " not found.");
        }
        return chocolate;
    }

    public Chocolate addChocolate(Chocolate chocolate) {
        if (chocolate == null) {
            throw new IllegalArgumentException("Chocolate cannot be null.");
        }
        // Additional validation can be added here
        chocolateDAO.save(chocolate); // Save the chocolate
        return chocolate; // Return the saved chocolate
    }

    public Chocolate updateChocolate(Chocolate chocolate) {
        if (chocolate == null) {
            throw new IllegalArgumentException("Chocolate cannot be null.");
        }
        // Additional validation can be added here
        chocolateDAO.change(chocolate); // Update the chocolate
        return chocolate; // Return the updated chocolate
    }

    public Chocolate deleteChocolate(int id) {
        Chocolate chocolate = chocolateDAO.findChocolate(id);
        if (chocolate == null) {
            // Handle the case when chocolate is not found
            throw new IllegalArgumentException("Chocolate with id " + id + " not found.");
        }
        chocolateDAO.delete(id); // Delete the chocolate
        return chocolate; // Return the deleted chocolate
    }
}
