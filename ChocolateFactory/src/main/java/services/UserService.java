package services;

import java.util.List;
import models.User;
import dao.UserDAO;

public class UserService {
    private UserDAO userDAO;

    public UserService() {
        userDAO = new UserDAO();
    }

    public List<User> getAllUsers() {
        return userDAO.getUsers();
    }

    public User getUserById(int id) {
        return userDAO.findUser(id);
    }

    public User addUser(User user) {
        return userDAO.save(user);
    }

    public User updateUser(User user) {
        return userDAO.save(user);
    }

    public User deleteUser(int id) {
        return userDAO.delete(id);
    }
}
