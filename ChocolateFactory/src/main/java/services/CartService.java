package services;

import java.util.List;
import models.Cart;
import dao.CartDAO;

public class CartService {
    private CartDAO cartDAO;

    public CartService() {
        cartDAO = new CartDAO();
    }

    public List<Cart> getAllCarts() {
        return cartDAO.getCarts();
    }

    public Cart getCartByUsername(String username) {
        return cartDAO.findCart(username);
    }

    public Cart addCart(Cart cart) {
        return cartDAO.save(cart);
    }

    public Cart updateCart(Cart cart) {
        return cartDAO.save(cart);
    }

    public Cart deleteCart(String username) {
        return cartDAO.delete(username);
    }
}
