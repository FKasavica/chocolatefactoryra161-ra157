package services;

import java.util.List;
import models.Factory;
import dao.FactoryDAO;

public class FactoryService {
    private FactoryDAO factoryDAO;

    public FactoryService() {
        factoryDAO = new FactoryDAO();
    }

    public List<Factory> getAllFactories() {
        return factoryDAO.getFactories();
    }

    public Factory getFactoryByName(String name) {
        return factoryDAO.findFactory(name);
    }

    public Factory addFactory(Factory factory) {
        return factoryDAO.save(factory);
    }

    public Factory updateFactory(Factory factory) {
        return factoryDAO.save(factory);
    }

    public Factory deleteFactory(String name) {
        return factoryDAO.delete(name);
    }
}
