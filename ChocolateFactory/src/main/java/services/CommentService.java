package services;

import java.util.List;
import models.Comment;
import dao.CommentDAO;

public class CommentService {
    private CommentDAO commentDAO;

    public CommentService() {
        commentDAO = new CommentDAO();
    }

    public List<Comment> getAllComments() {
        return commentDAO.getComments();
    }

    public Comment getComment(String username, String factoryName) {
        return commentDAO.findComment(username, factoryName);
    }

    public Comment addComment(Comment comment) {
        return commentDAO.save(comment);
    }

    public Comment updateComment(Comment comment) {
        return commentDAO.save(comment);
    }

    public Comment deleteComment(String username, String factoryName) {
        return commentDAO.delete(username, factoryName);
    }
}
