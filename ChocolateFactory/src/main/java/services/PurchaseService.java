package services;

import java.util.List;
import models.Purchase;
import dao.PurchaseDAO;

public class PurchaseService {
    private PurchaseDAO purchaseDAO;

    public PurchaseService() {
        purchaseDAO = new PurchaseDAO();
    }

    public List<Purchase> getAllPurchases() {
        return purchaseDAO.getPurchases();
    }

    public Purchase getPurchaseById(String id) {
        return purchaseDAO.findPurchase(id);
    }

    public Purchase addPurchase(Purchase purchase) {
        return purchaseDAO.save(purchase);
    }

    public Purchase updatePurchase(Purchase purchase) {
        return purchaseDAO.save(purchase);
    }

    public Purchase deletePurchase(String id) {
        return purchaseDAO.delete(id);
    }
}
