package dao;

import java.io.*;
import java.util.*;
import models.Cart;

public class CartDAO {
    private Map<String, Cart> carts;
    private String filePath = "data/cart.csv"; // Ažurirana putanja

    public CartDAO() {
        carts = new HashMap<>();
        loadCarts();
    }

    // Učitavanje podataka iz CSV fajla
    private void loadCarts() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                String username = fields[0];
                List<String> chocolateIds = Arrays.asList(fields[1].split(";"));
                double totalPrice = Double.parseDouble(fields[2]);
                Cart cart = new Cart(username);
                cart.setChocolateIds(chocolateIds);
                cart.setTotalPrice(totalPrice);
                carts.put(username, cart);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Čuvanje podataka u CSV fajl
    private void saveCarts() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(getClass().getClassLoader().getResource(filePath).toURI())))) {
            for (Cart cart : carts.values()) {
                String line = String.format("%s,%s,%f",
                    cart.getUsername(),
                    String.join(";", cart.getChocolateIds()),
                    cart.getTotalPrice()
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Cart> getCarts() {
        return new ArrayList<>(carts.values());
    }

    public Cart findCart(String username) {
        return carts.get(username);
    }

    public Cart save(Cart cart) {
        carts.put(cart.getUsername(), cart);
        saveCarts();
        return cart;
    }

    public Cart delete(String username) {
        Cart cart = carts.remove(username);
        saveCarts();
        return cart;
    }
}
