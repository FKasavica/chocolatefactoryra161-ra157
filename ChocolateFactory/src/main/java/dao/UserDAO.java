package dao;

import java.io.*;
import java.util.*;
import models.User;
import models.User.Role;
import models.User.CustomerType;

public class UserDAO {
    private Map<Integer, User> users;
    private String filePath = "data/users.csv";

    public UserDAO() {
        users = new HashMap<>();
        loadUsers();
    }

    private void loadUsers() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Role role = Role.valueOf(fields[7]);
                CustomerType customerType = CustomerType.valueOf(fields[9]);
                User user = new User(
                    Integer.parseInt(fields[0]),  // id
                    fields[1],  // username
                    fields[2],  // password
                    fields[3],  // firstName
                    fields[4],  // lastName
                    fields[5],  // gender
                    fields[6],  // dateOfBirth
                    role,
                    Integer.parseInt(fields[8]),  // points
                    customerType  // customerType
                );
                users.put(user.getId(), user);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<User> getUsers() {
        return new ArrayList<>(users.values());
    }

    public User findUser(int id) {
        return users.get(id);
    }

    public User save(User user) {
        users.put(user.getId(), user);
        saveUsers();
        return user;
    }

    public User delete(int id) {
        User user = users.remove(id);
        saveUsers();
        return user;
    }

    private void saveUsers() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(getClass().getClassLoader().getResource(filePath).getFile()))) {
            for (User user : users.values()) {
                String line = String.format("%d,%s,%s,%s,%s,%s,%s,%s,%d,%s",
                    user.getId(),
                    user.getUsername(),
                    user.getPassword(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getGender(),
                    user.getDateOfBirth(),
                    user.getRole().name(),
                    user.getPoints(),
                    user.getCustomerType().name()
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
