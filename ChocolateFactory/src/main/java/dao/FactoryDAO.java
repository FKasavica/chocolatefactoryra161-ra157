package dao;

import java.io.*;
import java.util.*;
import models.Factory;
import models.Factory.Status;
import models.Location;

public class FactoryDAO {
    private Map<String, Factory> factories;
    private String filePath = "data/factories.csv"; // Ažurirana putanja

    public FactoryDAO() {
        factories = new HashMap<>();
        loadFactories();
    }

    // Učitavanje podataka iz CSV fajla
    private void loadFactories() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Status status;
                try {
                    status = Status.valueOf(fields[2].toUpperCase());
                } catch (IllegalArgumentException e) {
                    status = Status.NOT_WORKING; // Default value if invalid
                }
                Location location = new Location(
                    Double.parseDouble(fields[3]),  // longitude
                    Double.parseDouble(fields[4]),  // latitude
                    fields[5]  // address
                );
                Factory factory = new Factory(
                    fields[0],  // name
                    fields[1],  // workingHours
                    status,  // status as enum
                    location,  // location
                    fields[6],  // logo
                    Double.parseDouble(fields[7])  // rating
                );
                factories.put(factory.getName(), factory);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Čuvanje podataka u CSV fajl
    private void saveFactories() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(getClass().getClassLoader().getResource(filePath).toURI())))) {
            for (Factory factory : factories.values()) {
                String line = String.format("%s,%s,%s,%f,%f,%s,%s,%f",
                    factory.getName(),
                    factory.getWorkingHours(),
                    factory.getStatus().name(), // Convert enum to string
                    factory.getLocation().getLongitude(),
                    factory.getLocation().getLatitude(),
                    factory.getLocation().getAddress(),
                    factory.getLogo(),
                    factory.getRating()
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Factory> getFactories() {
        return new ArrayList<>(factories.values());
    }

    public Factory findFactory(String name) {
        return factories.get(name);
    }

    public Factory save(Factory factory) {
        factories.put(factory.getName(), factory);
        saveFactories();
        return factory;
    }

    public Factory delete(String name) {
        Factory factory = factories.remove(name);
        saveFactories();
        return factory;
    }
}
