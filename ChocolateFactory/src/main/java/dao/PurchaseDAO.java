package dao;

import java.io.*;
import java.util.*;
import models.Purchase;
import models.Purchase.Status;

public class PurchaseDAO {
    private Map<String, Purchase> purchases;
    private String filePath = "data/purchases.csv"; // Ažurirana putanja

    public PurchaseDAO() {
        purchases = new HashMap<>();
        loadPurchases();
    }

    // Učitavanje podataka iz CSV fajla
    private void loadPurchases() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Status status;
                try {
                    status = Status.valueOf(fields[6].toUpperCase());
                } catch (IllegalArgumentException e) {
                    status = Status.PROCESSING; // Default value if invalid
                }
                List<String> chocolateIds = Arrays.asList(fields[1].split(";"));
                Purchase purchase = new Purchase(
                    fields[0],  // id
                    chocolateIds,  // chocolateIds
                    fields[2],  // factoryName
                    fields[3],  // dateTime
                    Double.parseDouble(fields[4]),  // totalPrice
                    fields[5],  // customerName
                    status  // status as enum
                );
                purchases.put(purchase.getId(), purchase);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Čuvanje podataka u CSV fajl
    private void savePurchases() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(getClass().getClassLoader().getResource(filePath).toURI())))) {
            for (Purchase purchase : purchases.values()) {
                String line = String.format("%s,%s,%s,%s,%f,%s,%s",
                    purchase.getId(),
                    String.join(";", purchase.getChocolateIds()),
                    purchase.getFactoryName(),
                    purchase.getDateTime(),
                    purchase.getTotalPrice(),
                    purchase.getCustomerName(),
                    purchase.getStatus().name() // Convert enum to string
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Purchase> getPurchases() {
        return new ArrayList<>(purchases.values());
    }

    public Purchase findPurchase(String id) {
        return purchases.get(id);
    }

    public Purchase save(Purchase purchase) {
        purchases.put(purchase.getId(), purchase);
        savePurchases();
        return purchase;
    }

    public Purchase delete(String id) {
        Purchase purchase = purchases.remove(id);
        savePurchases();
        return purchase;
    }
}
