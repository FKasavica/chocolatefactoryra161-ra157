package dao;

import java.io.*;
import java.util.*;
import models.Comment;
import models.Comment.Status;

public class CommentDAO {
    private Map<String, Comment> comments;
    private String filePath = "data/comments.csv"; // Ažurirana putanja

    public CommentDAO() {
        comments = new HashMap<>();
        loadComments();
    }

    // Učitavanje podataka iz CSV fajla
    private void loadComments() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Status status;
                try {
                    status = Status.valueOf(fields[4].toUpperCase());
                } catch (IllegalArgumentException e) {
                    status = Status.PENDING; // Default value if invalid
                }
                Comment comment = new Comment(
                    fields[0],  // username
                    fields[1],  // factoryName
                    fields[2],  // text
                    Integer.parseInt(fields[3]),  // rating
                    status  // status as enum
                );
                comments.put(fields[0] + "-" + fields[1], comment);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Čuvanje podataka u CSV fajl
    private void saveComments() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(getClass().getClassLoader().getResource(filePath).toURI())))) {
            for (Comment comment : comments.values()) {
                String line = String.format("%s,%s,%s,%d,%s",
                    comment.getUsername(),
                    comment.getFactoryName(),
                    comment.getText(),
                    comment.getRating(),
                    comment.getStatus().name() // Convert enum to string
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments.values());
    }

    public Comment findComment(String username, String factoryName) {
        return comments.get(username + "-" + factoryName);
    }

    public Comment save(Comment comment) {
        comments.put(comment.getUsername() + "-" + comment.getFactoryName(), comment);
        saveComments();
        return comment;
    }

    public Comment delete(String username, String factoryName) {
        Comment comment = comments.remove(username + "-" + factoryName);
        saveComments();
        return comment;
    }
}
