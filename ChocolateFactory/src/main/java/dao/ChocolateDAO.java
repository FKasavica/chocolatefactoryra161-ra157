package dao;

import java.io.*;
import java.util.*;
import models.Chocolate;
import models.Chocolate.ChocolateStatus;

public class ChocolateDAO {
    private Map<Integer, Chocolate> chocolates;
    private String filePath = "data/chocolates.csv"; // Ažurirana putanja

    public ChocolateDAO() {
        chocolates = new HashMap<>();
        loadChocolates();
    }

    private void loadChocolates() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                System.err.println("File not found: " + filePath);
                return;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Chocolate chocolate = new Chocolate(
                    Integer.parseInt(fields[0]),  // id
                    fields[1],  // name
                    Double.parseDouble(fields[2]),  // price
                    fields[3],  // type
                    fields[4],  // kind
                    Integer.parseInt(fields[5]),  // weight
                    fields[6],  // description
                    fields[7],  // image
                    Integer.parseInt(fields[8]),  // quantity
                    ChocolateStatus.valueOf(fields[9])  // status
                );
                chocolates.put(chocolate.getId(), chocolate);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Chocolate> getChocolates() {
        return new ArrayList<>(chocolates.values());
    }

    public Chocolate findChocolate(int id) {
        return chocolates.get(id);
    }

    public void save(Chocolate chocolate) {
        chocolates.put(chocolate.getId(), chocolate);
        saveChocolates();
    }

    public void change(Chocolate chocolate) {
        chocolates.put(chocolate.getId(), chocolate);
        saveChocolates();
    }

    public void delete(int id) {
        chocolates.remove(id);
        saveChocolates();
    }

    private void saveChocolates() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(getClass().getClassLoader().getResource(filePath).getFile()))) {
            for (Chocolate chocolate : chocolates.values()) {
                String line = String.format("%d,%s,%d,%s,%s,%d,%s,%s,%d,%s",
                    chocolate.getId(),
                    chocolate.getName(),
                    (int) chocolate.getPrice(),  // Convert price to integer
                    chocolate.getType(),
                    chocolate.getKind(),
                    chocolate.getWeight(),
                    chocolate.getDescription(),
                    chocolate.getImage(),
                    chocolate.getQuantity(),
                    chocolate.getStatus().name()
                );
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
