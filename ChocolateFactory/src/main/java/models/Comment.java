package models;

public class Comment {
    private String username; // Username of the user who made the comment
    private String factoryName; // Name of the factory being commented on
    private String text;
    private int rating; // Rating out of 5
    private Status status;

    public enum Status {
        PENDING, APPROVED, REJECTED;
    }

    public Comment(String username, String factoryName, String text, int rating, Status status) {
        this.username = username;
        this.factoryName = factoryName;
        this.text = text;
        this.rating = rating;
        this.status = status;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
