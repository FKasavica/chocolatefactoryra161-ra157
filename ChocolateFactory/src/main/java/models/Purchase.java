package models;

import java.util.List;

public class Purchase {
    private String id;
    private List<String> chocolateIds; // List of purchased chocolate IDs
    private String factoryName; // Name of the factory where the purchase was made
    private String dateTime;
    private double totalPrice;
    private String customerName;
    private Status status; // Purchase status

    public enum Status {
        PROCESSING, APPROVED, REJECTED, CANCELLED;
    }

    public Purchase(String id, List<String> chocolateIds, String factoryName, String dateTime, double totalPrice, String customerName, Status status) {
        this.id = id;
        this.chocolateIds = chocolateIds;
        this.factoryName = factoryName;
        this.dateTime = dateTime;
        this.totalPrice = totalPrice;
        this.customerName = customerName;
        this.status = status;
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getChocolateIds() {
        return chocolateIds;
    }

    public void setChocolateIds(List<String> chocolateIds) {
        this.chocolateIds = chocolateIds;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
