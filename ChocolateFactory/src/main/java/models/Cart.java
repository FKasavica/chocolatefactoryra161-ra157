package models;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private String username; // Username of the user who owns the cart
    private List<String> chocolateIds; // List of chocolate IDs in the cart
    private double totalPrice;

    public Cart(String username) {
        this.username = username;
        this.chocolateIds = new ArrayList<>();
        this.totalPrice = 0.0;
    }

    public Cart(String username, List<String> chocolateIds, double totalPrice) {
        this.username = username;
        this.chocolateIds = chocolateIds;
        this.totalPrice = totalPrice;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getChocolateIds() {
        return chocolateIds;
    }

    public void setChocolateIds(List<String> chocolateIds) {
        this.chocolateIds = chocolateIds;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    // Add chocolate to cart
    public void addChocolate(String chocolateId, double price) {
        this.chocolateIds.add(chocolateId);
        this.totalPrice += price;
    }

    // Remove chocolate from cart
    public void removeChocolate(String chocolateId, double price) {
        this.chocolateIds.remove(chocolateId);
        this.totalPrice -= price;
    }
}
