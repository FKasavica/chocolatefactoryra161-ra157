package models;

public class Chocolate {
    private int id;
    private String name;
    private double price;
    private String type;
    private String kind;
    private int weight;
    private String description;
    private String image;
    private int quantity;
    private ChocolateStatus status;

    public enum ChocolateStatus {
        AVAILABLE, OUT_OF_STOCK;
    }

    public Chocolate(int id, String name, double price, String type, String kind, int weight, String description, String image, int quantity, ChocolateStatus status) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.kind = kind;
        this.weight = weight;
        this.description = description;
        this.image = image;
        this.quantity = quantity;
        this.status = status;
    }

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight <= 0) {
            throw new IllegalArgumentException("Weight must be positive.");
        }
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 0) {
            throw new IllegalArgumentException("Quantity cannot be negative.");
        }
        this.quantity = quantity;
    }

    public ChocolateStatus getStatus() {
        return status;
    }

    public void setStatus(ChocolateStatus status) {
        this.status = status;
    }
}
