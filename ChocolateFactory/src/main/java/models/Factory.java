package models;

public class Factory {
    private String name;
    private String workingHours;
    private Status status; // Using enum for status
    private Location location;
    private String logo;
    private double rating;

    public enum Status {
        WORKING, NOT_WORKING;
    }

    public Factory(String name, String workingHours, Status status, Location location, String logo, double rating) {
        this.name = name;
        this.workingHours = workingHours;
        this.status = status;
        this.location = location;
        this.logo = logo;
        this.rating = rating;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
