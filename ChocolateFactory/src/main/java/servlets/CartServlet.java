package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Cart;
import services.CartService;

@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CartService cartService;

    public CartServlet() {
        super();
        cartService = new CartService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String usernameParam = request.getParameter("username");
        if (usernameParam != null) {
            Cart cart = cartService.getCartByUsername(usernameParam);
            if (cart != null) {
                response.setContentType("text/plain");
                response.getWriter().write(cartToText(cart));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            List<Cart> carts = cartService.getAllCarts();
            response.setContentType("text/plain");
            response.getWriter().write(cartsToText(carts));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Cart cart = textToCart(text);
        Cart createdCart = cartService.addCart(cart);
        response.setContentType("text/plain");
        response.getWriter().write(cartToText(createdCart));
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Cart cart = textToCart(text);
        Cart updatedCart = cartService.updateCart(cart);
        response.setContentType("text/plain");
        response.getWriter().write(cartToText(updatedCart));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String usernameParam = request.getParameter("username");
        if (usernameParam != null) {
            Cart deletedCart = cartService.deleteCart(usernameParam);
            if (deletedCart != null) {
                response.setContentType("text/plain");
                response.getWriter().write(cartToText(deletedCart));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // Helper methods to convert between Cart objects and plain text strings
    private String cartToText(Cart cart) {
        return String.format("%s,%s,%f",
                cart.getUsername(),
                String.join(";", cart.getChocolateIds()),
                cart.getTotalPrice());
    }

    private String cartsToText(List<Cart> carts) {
        StringBuilder text = new StringBuilder();
        for (Cart cart : carts) {
            text.append(cartToText(cart)).append("\n");
        }
        return text.toString();
    }

    private Cart textToCart(String text) {
        String[] fields = text.split(",");
        List<String> chocolateIds = Arrays.asList(fields[1].split(";"));
        return new Cart(
            fields[0],  // username
            chocolateIds,
            Double.parseDouble(fields[2])  // totalPrice
        );
    }
}
