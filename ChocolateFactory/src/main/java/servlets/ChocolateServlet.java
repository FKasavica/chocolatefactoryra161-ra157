package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Chocolate;
import models.Chocolate.ChocolateStatus;
import services.ChocolateService;
import java.util.List;


@WebServlet("/ChocolateServlet")
public class ChocolateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ChocolateService chocolateService;

    public ChocolateServlet() {
        super();
        chocolateService = new ChocolateService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet method called");
        String idParam = request.getParameter("id");
        if (idParam != null) {
            System.out.println("Received id: " + idParam);
            int id = Integer.parseInt(idParam);
            Chocolate chocolate = chocolateService.getChocolateById(id);
            if (chocolate != null) {
                response.setContentType("text/plain");
                response.getWriter().write(chocolateToText(chocolate));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            System.out.println("No id parameter found, fetching all chocolates");
            List<Chocolate> chocolates = chocolateService.getAllChocolates(); // Promenite tip na List<Chocolate>
            response.setContentType("text/plain");
            response.getWriter().write(chocolatesToText(chocolates));
        }
    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        StringBuilder text = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            text.append(line);
        }
        Chocolate chocolate = textToChocolate(text.toString());
        Chocolate createdChocolate = chocolateService.addChocolate(chocolate);
        response.setContentType("text/plain");
        response.getWriter().write(chocolateToText(createdChocolate));
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        StringBuilder text = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            text.append(line);
        }
        Chocolate chocolate = textToChocolate(text.toString());
        Chocolate updatedChocolate = chocolateService.updateChocolate(chocolate);
        response.setContentType("text/plain");
        response.getWriter().write(chocolateToText(updatedChocolate));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        if (idParam != null) {
            int id = Integer.parseInt(idParam);
            Chocolate deletedChocolate = chocolateService.deleteChocolate(id);
            if (deletedChocolate != null) {
                response.setContentType("text/plain");
                response.getWriter().write(chocolateToText(deletedChocolate));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // Helper methods to convert between Chocolate objects and plain text strings
    private String chocolateToText(Chocolate chocolate) {
        return String.format("%d,%s,%d,%s,%s,%d,%s,%s,%d,%s",
                chocolate.getId(),
                chocolate.getName(),
                (int) chocolate.getPrice(),  // Convert price to integer
                chocolate.getType(),
                chocolate.getKind(),
                chocolate.getWeight(),
                chocolate.getDescription(),
                chocolate.getImage(),
                chocolate.getQuantity(),
                chocolate.getStatus().name());
    }



    private String chocolatesToText(List<Chocolate> chocolates) {
        StringBuilder text = new StringBuilder();
        for (Chocolate chocolate : chocolates) {
            text.append(chocolateToText(chocolate)).append("\n");
        }
        return text.toString();
    }

    private Chocolate textToChocolate(String text) {
        String[] fields = text.split(",");
        ChocolateStatus status;
        try {
            status = ChocolateStatus.valueOf(fields[9].toUpperCase());
        } catch (IllegalArgumentException e) {
            status = ChocolateStatus.OUT_OF_STOCK; // Default value if status is invalid
        }
        try {
            return new Chocolate(
                Integer.parseInt(fields[0]),  // id
                fields[1],  // name
                Integer.parseInt(fields[2]),  // price in dinars
                fields[3],  // type
                fields[4],  // kind
                Integer.parseInt(fields[5]),  // weight
                fields[6],  // description
                fields[7],  // image
                Integer.parseInt(fields[8]),  // quantity
                status  // status as enum
            );
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid number format in input: " + text, e);
        }
    }


}
