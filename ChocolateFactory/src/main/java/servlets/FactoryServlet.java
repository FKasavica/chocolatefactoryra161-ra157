package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Factory;
import models.Factory.Status;
import models.Location;
import services.FactoryService;

@WebServlet("/FactoryServlet")
public class FactoryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private FactoryService factoryService;

    public FactoryServlet() {
        super();
        factoryService = new FactoryService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nameParam = request.getParameter("name");
        if (nameParam != null) {
            Factory factory = factoryService.getFactoryByName(nameParam);
            if (factory != null) {
                response.setContentType("text/plain");
                response.getWriter().write(factoryToText(factory));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            List<Factory> factories = factoryService.getAllFactories();
            response.setContentType("text/plain");
            response.getWriter().write(factoriesToText(factories));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Factory factory = textToFactory(text);
        Factory createdFactory = factoryService.addFactory(factory);
        response.setContentType("text/plain");
        response.getWriter().write(factoryToText(createdFactory));
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Factory factory = textToFactory(text);
        Factory updatedFactory = factoryService.updateFactory(factory);
        response.setContentType("text/plain");
        response.getWriter().write(factoryToText(updatedFactory));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nameParam = request.getParameter("name");
        if (nameParam != null) {
            Factory deletedFactory = factoryService.deleteFactory(nameParam);
            if (deletedFactory != null) {
                response.setContentType("text/plain");
                response.getWriter().write(factoryToText(deletedFactory));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // Helper methods to convert between Factory objects and plain text strings
    private String factoryToText(Factory factory) {
        return String.format("%s,%s,%s,%f,%f,%s,%s,%f",
                factory.getName(),
                factory.getWorkingHours(),
                factory.getStatus().name(),
                factory.getLocation().getLongitude(),
                factory.getLocation().getLatitude(),
                factory.getLocation().getAddress(),
                factory.getLogo(),
                factory.getRating());
    }

    private String factoriesToText(List<Factory> factories) {
        StringBuilder text = new StringBuilder();
        for (Factory factory : factories) {
            text.append(factoryToText(factory)).append("\n");
        }
        return text.toString();
    }

    private Factory textToFactory(String text) {
        String[] fields = text.split(",");
        Status status;
        try {
            status = Status.valueOf(fields[2].toUpperCase());
        } catch (IllegalArgumentException e) {
            status = Status.NOT_WORKING; // Default value if invalid
        }
        Location location = new Location(
            Double.parseDouble(fields[3]),
            Double.parseDouble(fields[4]),
            fields[5]
        );
        return new Factory(
            fields[0],  // name
            fields[1],  // workingHours
            status,  // status as enum
            location,  // location
            fields[6],  // logo
            Double.parseDouble(fields[7])  // rating
        );
    }
}
