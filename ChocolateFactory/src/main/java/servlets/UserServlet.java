package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.User;
import models.User.Role;
import models.User.CustomerType;
import services.UserService;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserService userService;

    public UserServlet() {
        super();
        userService = new UserService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Received GET request");
        String idParam = request.getParameter("id");
        System.out.println("ID parameter: " + idParam);
        if (idParam != null) {
            try {
                int id = Integer.parseInt(idParam);
                User user = userService.getUserById(id);
                if (user != null) {
                    response.setContentType("text/plain");
                    response.getWriter().write(userToText(user));
                } else {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            } catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null) {
            if (action.equals("register")) {
                registerUser(request, response);
            } else if (action.equals("login")) {
                loginUser(request, response);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
    
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        User user = textToUser(text);
        User updatedUser = userService.updateUser(user);
        response.setContentType("text/plain");
        response.getWriter().write(userToText(updatedUser));
    }

    private void registerUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        User user = textToUser(text);
        if (userService.getUserById(user.getId()) == null) {
            user.setRole(Role.CUSTOMER);
            user.setCustomerType(CustomerType.BRONZE);
            user.setPoints(0);
            User createdUser = userService.addUser(user);
            response.setContentType("text/plain");
            response.getWriter().write(userToText(createdUser));
        } else {
            response.setStatus(HttpServletResponse.SC_CONFLICT); // Conflict status if user already exists
        }
    }

    private void loginUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        String[] credentials = text.split(",");
        String username = credentials[0];
        String password = credentials[1];

        // Assume username is unique and try to find user by username
        User user = userService.getAllUsers().stream()
                .filter(u -> u.getUsername().equals(username))
                .findFirst()
                .orElse(null);

        if (user != null && user.getPassword().equals(password)) {
            response.setContentType("text/plain");
            response.getWriter().write(userToText(user));
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // Unauthorized status if credentials are invalid
        }
    }

    private String userToText(User user) {
        return String.format("ID: %d\nUsername: %s\nPassword: %s\nFirst Name: %s\nLast Name: %s\nGender: %s\nDate of Birth: %s\nRole: %s\nPoints: %d\nCustomer Type: %s",
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(),
                user.getGender(),
                user.getDateOfBirth(),
                user.getRole().name(),
                user.getPoints(),
                user.getCustomerType().name());
    }


    private User textToUser(String text) {
        String[] fields = text.split(",");
        return new User(
            Integer.parseInt(fields[0]),  // id
            fields[1],  // username
            fields[2],  // password
            fields[3],  // firstName
            fields[4],  // lastName
            fields[5],  // gender
            fields[6],  // dateOfBirth,
            Role.valueOf(fields[7]),
            Integer.parseInt(fields[8]),
            CustomerType.valueOf(fields[9])
        );
    }
}
