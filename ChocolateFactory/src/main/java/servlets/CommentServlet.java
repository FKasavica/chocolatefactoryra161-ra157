package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Comment;
import models.Comment.Status;
import services.CommentService;

@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private CommentService commentService;

    public CommentServlet() {
        super();
        commentService = new CommentService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String usernameParam = request.getParameter("username");
        String factoryNameParam = request.getParameter("factoryName");
        if (usernameParam != null && factoryNameParam != null) {
            Comment comment = commentService.getComment(usernameParam, factoryNameParam);
            if (comment != null) {
                response.setContentType("text/plain");
                response.getWriter().write(commentToText(comment));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            List<Comment> comments = commentService.getAllComments();
            response.setContentType("text/plain");
            response.getWriter().write(commentsToText(comments));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Comment comment = textToComment(text);
        Comment createdComment = commentService.addComment(comment);
        response.setContentType("text/plain");
        response.getWriter().write(commentToText(createdComment));
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Comment comment = textToComment(text);
        Comment updatedComment = commentService.updateComment(comment);
        response.setContentType("text/plain");
        response.getWriter().write(commentToText(updatedComment));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String usernameParam = request.getParameter("username");
        String factoryNameParam = request.getParameter("factoryName");
        if (usernameParam != null && factoryNameParam != null) {
            Comment deletedComment = commentService.deleteComment(usernameParam, factoryNameParam);
            if (deletedComment != null) {
                response.setContentType("text/plain");
                response.getWriter().write(commentToText(deletedComment));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // Helper methods to convert between Comment objects and plain text strings
    private String commentToText(Comment comment) {
        return String.format("%s,%s,%s,%d,%s",
                comment.getUsername(),
                comment.getFactoryName(),
                comment.getText(),
                comment.getRating(),
                comment.getStatus().name());
    }

    private String commentsToText(List<Comment> comments) {
        StringBuilder text = new StringBuilder();
        for (Comment comment : comments) {
            text.append(commentToText(comment)).append("\n");
        }
        return text.toString();
    }

    private Comment textToComment(String text) {
        String[] fields = text.split(",");
        Status status;
        try {
            status = Status.valueOf(fields[4].toUpperCase());
        } catch (IllegalArgumentException e) {
            status = Status.PENDING; // Default value if invalid
        }
        return new Comment(
            fields[0],  // username
            fields[1],  // factoryName
            fields[2],  // text
            Integer.parseInt(fields[3]),  // rating
            status  // status as enum
        );
    }
}
