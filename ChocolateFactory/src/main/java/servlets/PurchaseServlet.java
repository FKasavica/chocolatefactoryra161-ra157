package servlets;

import java.io.IOException;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Purchase;
import models.Purchase.Status;
import services.PurchaseService;

@WebServlet("/PurchaseServlet")
public class PurchaseServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private PurchaseService purchaseService;

    public PurchaseServlet() {
        super();
        purchaseService = new PurchaseService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        if (idParam != null) {
            Purchase purchase = purchaseService.getPurchaseById(idParam);
            if (purchase != null) {
                response.setContentType("text/plain");
                response.getWriter().write(purchaseToText(purchase));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            List<Purchase> purchases = purchaseService.getAllPurchases();
            response.setContentType("text/plain");
            response.getWriter().write(purchasesToText(purchases));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Purchase purchase = textToPurchase(text);
        Purchase createdPurchase = purchaseService.addPurchase(purchase);
        response.setContentType("text/plain");
        response.getWriter().write(purchaseToText(createdPurchase));
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String text = reader.lines().collect(Collectors.joining());
        Purchase purchase = textToPurchase(text);
        Purchase updatedPurchase = purchaseService.updatePurchase(purchase);
        response.setContentType("text/plain");
        response.getWriter().write(purchaseToText(updatedPurchase));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        if (idParam != null) {
            Purchase deletedPurchase = purchaseService.deletePurchase(idParam);
            if (deletedPurchase != null) {
                response.setContentType("text/plain");
                response.getWriter().write(purchaseToText(deletedPurchase));
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // Helper methods to convert between Purchase objects and plain text strings
    private String purchaseToText(Purchase purchase) {
        return String.format("%s,%s,%s,%s,%f,%s,%s",
                purchase.getId(),
                String.join(";", purchase.getChocolateIds()),
                purchase.getFactoryName(),
                purchase.getDateTime(),
                purchase.getTotalPrice(),
                purchase.getCustomerName(),
                purchase.getStatus().name());
    }

    private String purchasesToText(List<Purchase> purchases) {
        StringBuilder text = new StringBuilder();
        for (Purchase purchase : purchases) {
            text.append(purchaseToText(purchase)).append("\n");
        }
        return text.toString();
    }

    private Purchase textToPurchase(String text) {
        String[] fields = text.split(",");
        List<String> chocolateIds = Arrays.asList(fields[1].split(";"));
        Status status;
        try {
            status = Status.valueOf(fields[6].toUpperCase());
        } catch (IllegalArgumentException e) {
            status = Status.PROCESSING; // Default value if invalid
        }
        return new Purchase(
            fields[0],  // id
            chocolateIds,
            fields[2],  // factoryName
            fields[3],  // dateTime
            Double.parseDouble(fields[4]),  // totalPrice
            fields[5],  // customerName
            status  // status as enum
        );
    }
}
