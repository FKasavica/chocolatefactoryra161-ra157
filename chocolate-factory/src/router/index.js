import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import FactoryDetail from '../components/FactoryDetail.vue';
import ChocolateForm from '../components/ChocolateForm.vue';
import LoginForm from '../components/LoginForm.vue';

const routes = [
  {
    path: '/',
    name: 'Login',
    component: LoginForm
  },
  {
    path: '/home',
    name: 'Home',
    component: HomeView
  },
  {
    path: '/factory/:id',
    name: 'FactoryDetail',
    component: FactoryDetail
  },
  {
    path: '/add-chocolate/:factoryId',
    name: 'AddChocolate',
    component: ChocolateForm
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

export default router;
